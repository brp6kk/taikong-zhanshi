﻿using System.Drawing;
using System.Windows.Forms;

namespace Taikong_Zhanshi
{
    abstract class Bug
    {
        // Bug images.
        protected int CurrentFrame { get; set; }
        protected Image[] Frames { get; set; }
        public PictureBox Box { get; protected set; }

        public bool Alive { get; private set; } = true;
        
        // Movement properties.
        private bool movingLeft = true;
        public bool MovingLeft
        {
            get
            {
                return (movingLeft);
            }
            set
            {
                if (this.Alive)
                {
                    if (value && this.MovingRight)
                    {
                        this.MovingRight = false;
                    }
                    this.movingLeft = value;
                }
            }
        }
        private bool movingRight = false;
        public bool MovingRight
        {
            get
            {
                return (movingRight);
            }
            set
            {
                if (this.Alive)
                {
                    if (value && this.MovingLeft)
                    {
                        this.MovingLeft = false;
                    }
                    this.movingRight = value;
                }
            }
        }

        // Attacking property.
        public bool Attacking { get; set; } = false;

        // Constructor.
        public Bug (int startFrame, Point location)
        {
            this.Box = new PictureBox { Size = new Size(25, 25), Location = location };
            this.CurrentFrame = startFrame;
            
        }

        // Default bug image - attacking bug should only ever display this image.
        public Image DefaultImage()
        {
            return (this.Frames[0]);
        }

        // Changes bug image to the next frame in the animation.
        public void NextFrame()
        {
            if (++CurrentFrame > 2)
                CurrentFrame = 0;

            if (!this.Attacking)
                Box.Image = Frames[CurrentFrame];
        }

        // Kills bug so that it can no longer be active.
        public void Kill()
        {
            this.Alive = false;
            this.Attacking = false;
        }

        // Moves the bug appropriately.
        public void Move()
        {
            // Bug only moves if it's alive.
            if (this.Alive)
            {
                // Different movement for an attacking vs non-attacking bug.
                if (this.Attacking)
                {
                    if (this.MovingLeft)
                        this.MoveX(-Settings.EnemyAttackXMove);
                    else if (this.MovingRight)
                        this.MoveX(Settings.EnemyAttackXMove);

                    // Attacking bug moves on its own, so it changes directions on its own
                    // rather than as a group.
                    if (!Settings.InXBounds(this.Box.Location.X))
                    {
                        this.ChangeDirection();
                    }

                    this.MoveY(Settings.EnemyAttackYMove);
                }
                else
                {
                    if (this.MovingLeft)
                        this.MoveX(-Settings.ENEMY_NON_ATTACK_MOVE);
                    else if (this.MovingRight)
                        this.MoveX(Settings.ENEMY_NON_ATTACK_MOVE);
                }
            }
        }

        // Changes bug's x-direction.
        private void ChangeDirection()
        {
            if (this.MovingLeft) this.MovingRight = true;
            else if (this.MovingRight) this.MovingLeft = true;
        }

        // Moves the bug horizontally.
        private void MoveX(int inc)
        {
            Point currentLocation = this.Box.Location;
            Point newLocation = Point.Add(currentLocation, new Size(inc, 0));

            this.Box.Location = newLocation;
        }

        // Moves the bug vertically.
        private void MoveY (int inc)
        {
            Point currentLocation = this.Box.Location;
            Point newLocation = Point.Add(currentLocation, new Size(0, inc));

            this.Box.Location = newLocation;
        }

        // Shoots a bullet originating from this bug.
        public Bullet Shoot()
        {
            return (new BugBullet(this));
        }
    }
}