﻿using System.Drawing;
using System.Windows.Forms;

namespace Taikong_Zhanshi
{
    class PlayerBullet : Bullet
    {
        // Constructor.
        public PlayerBullet(Player player) : base(Properties.Resources.Shoot)
        {
            this.Box.Location = this.StartingLocation(player.Box);
        }

        // Moves bullet vertically away from the player.
        public override void MoveBullet()
        {
            this.Box.Location = new Point(this.Box.Location.X, this.Box.Location.Y + Settings.PLAYER_BULLET_MOVE);
        }

        // Positions bullet to start from the tip of the player's ship.
        public override Point StartingLocation(PictureBox reference)
        {
            int referenceX = reference.Location.X;
            int referenceY = reference.Location.Y;
            int x = referenceX + (Settings.PLAYER_WIDTH / 2) - (Settings.BULLET_WIDTH / 2);
            int y = referenceY - Settings.BULLET_HEIGHT;

            return (new Point(x, y));
        }
    }
}