﻿using System.Collections.Generic;
using System.IO;

namespace Taikong_Zhanshi
{
    class SaveData
    {
        // Properties.
        public string FileName { get; private set; }
        public List<SavedScore> SavedScores { get; private set; } = new List<SavedScore>();

        // Constructor.
        public SaveData(string fileName)
        {
            this.FileName = fileName;
        }

        // Reads in file with name FileName and stores the scores in the SavedScores list.
        public void ReadFile()
        {
            FileStream stream;
            
            // Tries to open the file - if no such file exists, this method is left
            // and the scores list starts out empty.
            try
            {
                stream = new FileStream(FileName, FileMode.Open, FileAccess.Read);
            }
            catch (FileNotFoundException)
            {
                return;
            }

            StreamReader reader = new StreamReader(stream);

            // Reads entirety of file.
            string lines = reader.ReadToEnd();

            reader.Close();
            stream.Close();

            // Converts contents of file to list of scores, saving them to SavedScores.
            this.SavedScores = SavedScore.StringToScores(lines);
        }

        // Writes the top 10 scores to a file named FileName.
        public void WriteFile()
        {
            string lines = this.FormatFileScores();

            FileStream stream = new FileStream(this.FileName, FileMode.Create, FileAccess.Write);
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(lines);
            writer.Close();
            stream.Close();
        }

        // Sorts scores in descending order.
        public void SortScores()
        {
            bool sortAgain;

            // Loops until scores are sorted.
            // Bubble sort algorithm.
            do
            {
                sortAgain = false;
                for (int index = 0; index < this.SavedScores.Count - 1; index++)
                {
                    if (this.SavedScores[index].Score < this.SavedScores[index+1].Score)
                    {
                        SavedScore temp = this.SavedScores[index];
                        this.SavedScores[index] = this.SavedScores[index + 1];
                        this.SavedScores[index + 1] = temp;
                        sortAgain = true;
                    }
                }
            } while (sortAgain);
        }

        // Sorts scores and removes any below top 10.
        public void TopTenScores()
        {
            this.SortScores();

            for (int index = 10; index < this.SavedScores.Count; index++)
            {
                this.SavedScores.RemoveAt(index--);
            }
        }

        // Sort scores descending by score, take top 10, return as CSV string
        public string FormatFileScores()
        {
            this.TopTenScores();

            return (SavedScore.ScoresToString(this.SavedScores));
        }

        // Adds trailing 0 to number.
        public string FormatNumber(int num)
        {
            string str = num.ToString();
            int zeros = 2 - str.Length;

            for (int index = 0; index < zeros; index++)
            {
                str = "0" + str;
            }

            return (str);
        }

        // Formats scores to display on a form.
        public string FormatFormScores()
        {
            this.TopTenScores();
            string[] lines = new string[10];

            // Scores saved.
            int index;
            for (index = 0; index < this.SavedScores.Count; index++)
            {
                lines[index] = (FormatNumber(index + 1)) + " - " + this.SavedScores[index].Name + " - " + this.SavedScores[index].Score;
            }

            // Additional lines, if number of scores saved is less than 10.
            for (; index < lines.Length; index++)
            {
                lines[index] = (FormatNumber(index + 1)) + " - AAA - 0";
            }

            // Add newlines.
            for (index = 0; index < lines.Length - 1; index++)
            {
                lines[index] += "\n";
            }

            // Create actual string.
            string str = "";
            foreach (string line in lines)
            {
                str += line;
            }

            return (str);
        }

        // Determines if the passed score is a new high score.
        public bool HighScore(int score)
        {
            this.SortScores();

            return (this.SavedScores.Count < 10 || score > this.SavedScores[9].Score);
        }

        // Adds a new score to the saved scores.
        public void NewScore(SavedScore newScore)
        {
            this.SavedScores.Add(newScore);
        }
    }
}