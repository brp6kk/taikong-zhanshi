﻿using System.Drawing;

namespace Taikong_Zhanshi
{
    class PinkBug : Bug
    {
        // Constructor.
        public PinkBug(int startFrame, Point location) : base(startFrame, location)
        {
            Frames = new Image[] { Properties.Resources.Pink_Bug_1, Properties.Resources.Pink_Bug_2, Properties.Resources.Pink_Bug_3 };
            this.Box.Image = Frames[CurrentFrame];
        }
    }
}