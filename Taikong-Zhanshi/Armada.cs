﻿using System;
using System.Drawing;

namespace Taikong_Zhanshi
{
    class Armada
    {
        // Container of bugs, plus a placeholder bug.
        public Bug[][] Army { get; set; }
        public Bug Temp { get; private set; } = null;

        // Movement properties.
        private bool movingLeft = true;
        public bool MovingLeft
        {
            get
            {
                return (movingLeft);
            }
            set
            {
                // Can't move left and right at the same time.
                if (value && this.MovingRight)
                {
                    this.MovingRight = false;
                }

                // Changes direction of all non-attacking bugs in army.
                foreach (Bug[] row in this.Army)
                {
                    foreach (Bug bug in row)
                    {
                        if (!bug.Attacking)
                        {
                            bug.MovingLeft = value;
                        }
                    }
                }

                // Changes direction of placeholder bug if it has a value.
                if (this.Temp != null)
                {
                    this.Temp.MovingLeft = true;
                }

                movingLeft = value;
            }
        }
        private bool movingRight = false;
        public bool MovingRight
        {
            get
            {
                return (movingRight);
            }
            set
            {
                // Can't move left and right at the same time.
                if (value && this.MovingLeft)
                {
                    this.MovingLeft = false;
                }

                // Changes direction of all non-attacking bugs in army.
                foreach (Bug[] row in this.Army)
                {
                    foreach (Bug bug in row)
                    {
                        if (!bug.Attacking)
                        {
                            bug.MovingRight = value;
                        }
                    }
                }

                // Changes direction of placeholder bug if it has a value.
                if (this.Temp != null)
                {
                    this.Temp.MovingRight = true;
                }

                movingRight = value;
            }
        }

        // Constructor.
        public Armada(Bug[][] army)
        {
            this.Army = army;
        }

        // Number of non-null bugs in army.
        public int Size()
        {
            int count = 0;
            foreach (Bug[] row in Army)
            {
                foreach (Bug bug in row)
                {
                    count++;
                }
            }
            return (count);
        }

        // Number of alive bugs in army.
        public int NumAlive()
        {
            int count = 0;
            foreach (Bug[] row in Army)
            {
                foreach (Bug bug in row)
                {
                    if (bug.Alive)
                    {
                        count++;
                    }
                }
            }
            return (count);
        }

        // Number of rows in army.
        public int NumRows()
        {
            return (this.Army.Length);
        }

        // Number of columns in a row of army.
        public int NumColumns(int rowIndex)
        {
            return (this.Army[rowIndex].Length);
        }

        // Updates images for all bugs in the armada.
        public void UpdateFrames()
        {
            foreach (Bug[] row in this.Army)
            {
                foreach (Bug bug in row)
                {
                    bug.NextFrame();
                }
            }
        }

        // Gets the bug in the armada that is currently attacking, if it exists.
        public Bug AttackingBug()
        {
            foreach (Bug[] row in Army)
            {
                foreach (Bug bug in row)
                {
                    if (bug.Attacking)
                    {
                        return (bug);
                    }
                }
            }

            return (null);
        }

        // Returns true if the armada contains an attacking bug.
        public bool CurrentlyAttacking()
        {
            return (this.AttackingBug() != null);
        }

        // Starts a new attack, assuming that there is no other attack has already started.
        public void StartAttack()
        {
            if (!this.CurrentlyAttacking())
            {
                Random rand = new Random();
                bool attackStarted = false;

                // Randomly choose bugs until an alive one is found.
                // The first random alive bug is set to attack.
                do
                {
                    int row = rand.Next(0, this.NumRows());
                    int column = rand.Next(0, this.NumColumns(row));
                    Bug randBug = this.Army[row][column];
                    if (randBug.Alive)
                    {
                        this.Temp = new PurpleBug(0, randBug.Box.Location);
                        if (randBug.MovingRight) this.Temp.MovingRight = true;
                        randBug.Attacking = true;
                        randBug.Box.Image = randBug.DefaultImage();
                        attackStarted = true;
                    }
                } while (!attackStarted);
            }
        }

        // Change the movement direction of the whole armada.
        private void ChangeDirection()
        {
            if (this.MovingLeft)
                this.MovingRight = true;
            else if (this.MovingRight) 
                this.MovingLeft = true;
        }

        // Move all bugs in the armada.
        public void Move()
        {
            bool changeDirection = false;

            // Moves all bugs.
            foreach (Bug[] row in this.Army)
            {
                foreach (Bug bug in row)
                {
                    if (bug != null && bug.Alive)
                    {
                        bug.Move();

                        // The entire army has to move in the other direction if at least one bug leaves the form's bounds.
                        if (!bug.Attacking && !Settings.InXBounds(bug.Box.Location.X))
                            changeDirection = true;
                        // The attacking bug is the only one that can possibly leave the y-bounds, and this needs to be checked.
                        else if (bug.Attacking && !Settings.InYBounds(bug.Box.Location.Y))
                        {
                            // Move attacking bug to it's original position - this bug is now no longer the attacking bug.
                            bug.Attacking = false;
                            bug.Box.Location = Temp.Box.Location;
                            if (this.MovingLeft) bug.MovingLeft = true;
                            else if (this.MovingRight) bug.MovingRight = true;
                            bug.Move();
                            this.Temp = null;
                        }
                    }
                }
            }

            // Moves the placeholder for the attacking bug.
            if (this.Temp != null)
            {
                this.Temp.Move();
                if (!Settings.InXBounds(this.Temp.Box.Location.X))
                    changeDirection = true;
            }

            if (changeDirection)
            {
                this.ChangeDirection();
            }
        }

        // Creates and returns the armada necessary for this game.
        public static Armada BuildDefaultArmada()
        {
            Bug[][] army = new Bug[5][];
            int xCalc(int x) { return (20 + 25 * x); }
            int yCalc(int y) { return (0 + 25 * y); }

            // First row - 6 green bugs
            army[0] = new Bug[6];
            for (int column = 0; column < 6; column++)
            {
                army[0][column] = new GreenBug((column + 2) % 3, new Point(xCalc(column + 2), yCalc(0)));
            }

            // Second row - 8 pink bugs
            army[1] = new Bug[8];
            for (int column = 0; column < 8; column++)
            {
                army[1][column] = new PinkBug((column + 1) % 3, new Point(xCalc(column + 1), yCalc(1)));
            }

            // Third-fifth rows - 10 purple bugs each
            for (int row = 2; row < army.Length; row++)
            {
                army[row] = new Bug[10];
                for (int column = 0; column < 10; column++)
                {
                    army[row][column] = new PurpleBug(column % 3, new Point(xCalc(column), yCalc(row)));
                }
            }

            return (new Armada(army));
        }
    }
}