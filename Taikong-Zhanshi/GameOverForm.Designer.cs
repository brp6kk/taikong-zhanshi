﻿namespace Taikong_Zhanshi
{
    partial class GameOverForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GameOverForm));
            this.lblPrompt = new System.Windows.Forms.Label();
            this.txtEnterName = new System.Windows.Forms.TextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblStats = new System.Windows.Forms.Label();
            this.lblHighScorePrompt = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblPrompt
            // 
            this.lblPrompt.Font = new System.Drawing.Font("OCR A Extended", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrompt.Location = new System.Drawing.Point(0, 14);
            this.lblPrompt.Name = "lblPrompt";
            this.lblPrompt.Size = new System.Drawing.Size(169, 92);
            this.lblPrompt.TabIndex = 0;
            this.lblPrompt.Text = "Score:\r\nAccuracy:";
            this.lblPrompt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtEnterName
            // 
            this.txtEnterName.BackColor = System.Drawing.Color.Black;
            this.txtEnterName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEnterName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEnterName.Font = new System.Drawing.Font("OCR A Extended", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEnterName.ForeColor = System.Drawing.Color.Lime;
            this.txtEnterName.Location = new System.Drawing.Point(122, 233);
            this.txtEnterName.MaxLength = 3;
            this.txtEnterName.Name = "txtEnterName";
            this.txtEnterName.Size = new System.Drawing.Size(90, 70);
            this.txtEnterName.TabIndex = 1;
            this.txtEnterName.Text = "AAA";
            this.txtEnterName.Visible = false;
            // 
            // btnClose
            // 
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Location = new System.Drawing.Point(82, 349);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(170, 50);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.ClickClose);
            this.btnClose.Enter += new System.EventHandler(this.HighlightButton);
            this.btnClose.Leave += new System.EventHandler(this.UnhighlightButton);
            // 
            // lblStats
            // 
            this.lblStats.Font = new System.Drawing.Font("OCR A Extended", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStats.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblStats.Location = new System.Drawing.Point(177, 14);
            this.lblStats.Name = "lblStats";
            this.lblStats.Size = new System.Drawing.Size(155, 92);
            this.lblStats.TabIndex = 3;
            this.lblStats.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblHighScorePrompt
            // 
            this.lblHighScorePrompt.Location = new System.Drawing.Point(12, 151);
            this.lblHighScorePrompt.Name = "lblHighScorePrompt";
            this.lblHighScorePrompt.Size = new System.Drawing.Size(310, 119);
            this.lblHighScorePrompt.TabIndex = 4;
            this.lblHighScorePrompt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GameOverForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(334, 411);
            this.ControlBox = false;
            this.Controls.Add(this.lblHighScorePrompt);
            this.Controls.Add(this.lblStats);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.txtEnterName);
            this.Controls.Add(this.lblPrompt);
            this.Font = new System.Drawing.Font("OCR A Extended", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(7, 5, 7, 5);
            this.MaximizeBox = false;
            this.Name = "GameOverForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "太空战士";
            this.Load += new System.EventHandler(this.FormLoad);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PressKey);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPrompt;
        private System.Windows.Forms.TextBox txtEnterName;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblStats;
        private System.Windows.Forms.Label lblHighScorePrompt;
    }
}