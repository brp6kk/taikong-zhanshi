﻿using System;
using System.Collections.Generic;

namespace Taikong_Zhanshi
{
    class SavedScore
    {
        // Properties.
        public string Name { get; set; }
        public int Score { get; set; }

        // Constructors.
        public SavedScore(string name, int score)
        {
            this.Name = name;
            this.Score = score;
        }

        // Converts a string input to a list of SavedScores.
        // Expected format: [Name],[Score]\n.
        public static List<SavedScore> StringToScores(string input)
        {
            List<SavedScore> scores = new List<SavedScore>();
            string[] lines = input.Split('\n');
            bool badFormat = false;

            for (int index = 0; index < lines.Length && !badFormat; index++)
            {
                string[] components = lines[index].Split(',');

                try
                {
                    scores.Add(new SavedScore(components[0], Convert.ToInt32(components[1])));
                }
                catch (Exception)
                {
                    badFormat = true;
                }
            }
            return (scores);
        }

        // Converts a list of SavedScores to a string.
        // Output format: [Name],[Score]\n.
        public static string ScoresToString(List<SavedScore> scores)
        {
            string str = "";

            foreach (SavedScore score in scores)
            {
                str += score.Name + ',' + score.Score + '\n';
            }

            return (str);
        }
    }
}
