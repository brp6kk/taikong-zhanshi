﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Taikong_Zhanshi
{
    public partial class TestForm : Form
    {
        Stage stage;

        public TestForm()
        {
            InitializeComponent();
        }

        private void TestForm_Load(object sender, EventArgs e)
        {
            stage = new Stage(new Player(), this);
            stage.OnLoad();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            stage.Tick1Actions();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            stage.Tick2Actions();
            if (stage.GameOver())
            {
                Form form = new Form { Size = new Size(300, 300) };
                form.Show();
                form.Controls.Add(new Label { Text = "Score: " + Settings.Score + "\nLevels cleared: " + Settings.NumStagesCleared + "\nBugs left: " + stage.Army.NumAlive() + "\nNum shots: " + Settings.PlayerShotsFired + "\nAccuracy: " + Settings.PlayerShotAccuracy(stage.Army.NumAlive())
               , Size = new Size(100,300)});
                
                Settings.ResetSettings();
                this.Dispose();
            }
            else if (stage.Complete())
            {
                stage = stage.NextStage();
                GC.Collect();
            }
        }

        private void TestForm_KeyDown(object sender, KeyEventArgs e)
        {
            stage.KeyDownActions(e.KeyCode);
        }

        private void TestForm_KeyUp(object sender, KeyEventArgs e)
        {
            stage.KeyUpActions(e.KeyCode);
        }
    }
}
