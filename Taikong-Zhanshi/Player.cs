﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Taikong_Zhanshi
{
    class Player
    {
        // Default player image.
        private static readonly Image Image = Properties.Resources.Rocket;
        public PictureBox Box { get; private set; } = new PictureBox { Size = new Size(Settings.PLAYER_WIDTH,Settings.PLAYER_HEIGHT), Location = new Point(175, 340),
                                                                       Image = Player.Image, SizeMode = PictureBoxSizeMode.CenterImage};

        // Properties involving death animation.
        private int AnimationCounter { get; set; } = 0;
        private static readonly Image[] ExplosionImages = { Properties.Resources.explosion, Properties.Resources.explosion2,
                                                            Properties.Resources.explosion3 };

        // Pictureboxes representing number of lives left.
        public List<PictureBox> LifeMarkers { get; set; } = new List<PictureBox>() { };

        public int Lives { get; private set; } = 3;
        public int Score { get; private set; } = 0;

        // Movement properties.
        private bool movingLeft = false;
        public bool MovingLeft
        {
            get
            {
                return (movingLeft);
            }
            set
            {
                if (this.CanMove)
                    movingLeft = value;
            }
        }
        private bool movingRight = false;
        public bool MovingRight
        {
            get
            {
                return (movingRight);
            }
            set
            {
                if (this.CanMove)
                    movingRight = value;
            }
        }
        private bool canMove = true;
        public bool CanMove
        {
            get
            {
                return (canMove);
            }
            set
            {
                if (!value)
                {
                    this.MovingLeft = false;
                    this.MovingRight = false;
                    this.CanShoot = false;
                }

                canMove = value;
            }
        }

        // Attacking property.
        private bool canShoot = true;
        public bool CanShoot
        {
            get
            {
                return (canShoot);
            }
            set
            {
                if (this.CanMove)
                    canShoot = value;
            }
        }

        // Constructor.
        public Player()
        {
            this.CreateLifeMarkers();
        }

        // Moves player appropriately.
        public void Move()
        {
            if (this.MovingLeft)
            {
                this.MoveX(-5);
            }
            else if (this.MovingRight)
            {
                this.MoveX(5);
            }
        }

        // Moves player horizontally, ensuring it stays within the form's bounds.
        private void MoveX(int inc)
        {
            Point currentLocation = this.Box.Location;
            Point newLocation = Point.Add(currentLocation, new Size(inc, 0));

            if (newLocation.X > Settings.PLAYER_MIN_X  && newLocation.X < Settings.PLAYER_MAX_X)
                this.Box.Location = newLocation;
        }

        public Bullet Shoot()
        {
            if (this.CanShoot)
            {
                return (new PlayerBullet(this));
            }

            return (null);
        }

        // Actions that should occur when player uses a life.
        public void Kill()
        {
            // Dead players can't move.
            this.CanMove = false;

            // Resets animation counter.
            this.AnimationCounter = 0;

            // Starts explosion animation.
            Timer animationTimer = new Timer { Interval = 100 };
            animationTimer.Tick += new EventHandler(this.ExplosionAnimation);
            animationTimer.Enabled = true;

            // Removes lifemarker assuming that any still exist.
            if (this.LifeMarkers.Count > 0)
            {
                this.LifeMarkers.RemoveAt(this.LifeMarkers.Count - 1);
            }
        }

        // Actions that should execute on every tick of a timer to handle the animation of the player's death.
        public void ExplosionAnimation(object Sender, EventArgs e)
        {
            // Index of animation image is determined based on animation counter.
            // Note that this is done rather than setting interval to 400 and directly
            // using AnimationCounter because doing that results in a longer delay
            // before the beginning of the animation which I am not a fan of.
            int index = this.AnimationCounter / Settings.EXPLOSION_ANIMATION_INVERVAL;

            // Updates image as long as the explosion image exists.
            if (index < Player.ExplosionImages.Length)
            {
                this.Box.Image = Player.ExplosionImages[index];
                this.AnimationCounter++;
            }
            else
            {
                this.Lives--;

                // Resets Properties so that player can continue playing.
                this.Box.Image = Player.Image;
                this.CanMove = true;
                this.CanShoot = true;

                ((Timer)Sender).Enabled = false;
                ((Timer)Sender).Dispose();
            }
        }

        // Creates pictureboxes that represent the number of player's lives.
        private void CreateLifeMarkers()
        {
            // Number of markers is one less than the actual number of lives.
            for (int index = 0; index < this.Lives - 1; index++)
            {
                this.LifeMarkers.Add(new PictureBox {
                    Location = new Point(Settings.LIFE_MARKER_X * (index + 1) + Settings.LIFE_MARKER_WIDTH * index, Settings.LIFE_MARKER_Y),
                    Size = new Size(Settings.LIFE_MARKER_WIDTH, Settings.LIFE_MARKER_HEIGHT),
                    Image = Player.Image,
                    SizeMode = PictureBoxSizeMode.Zoom });
            }
        }

        // Space that player occupies on the form.
        // Separate from this.Box.Bounds because the PictureBox is larger than the player
        // to account for the larger explosion images.
        public Rectangle Bounds()
        {
            PictureBox box = new PictureBox { Size = new Size(Settings.PLAYER_WIDTH_REAL, Settings.PLAYER_HEIGHT),
                                              Location = new Point(this.Box.Location.X + 12, this.Box.Location.Y) };

            return (box.Bounds);
        }
    }
}