﻿using System.Drawing;

namespace Taikong_Zhanshi
{
    class GreenBug : Bug
    {
        // Constructor.
        public GreenBug(int startFrame, Point location) : base (startFrame, location)
        {
            Frames = new Image[] { Properties.Resources.Green_Bug_1, Properties.Resources.Green_Bug_2, Properties.Resources.Green_Bug_3 };
            this.Box.Image = Frames[CurrentFrame];
        }
    }
}