﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Taikong_Zhanshi
{
    public partial class LeaderboardForm : Form
    {
        // Constructor.
        public LeaderboardForm()
        {
            InitializeComponent();
        }

        // Event handler - executes code to start up the form.
        // Meant to be called on a form load.
        private void FormLoad(object sender, EventArgs e)
        {
            SaveData data = new SaveData(Settings.FILENAME);
            data.ReadFile();
            lblBoard.Text = data.FormatFormScores();
        }

        // Event handler - changes backcolor of button to purple.
        // Meant to be called when focus is shifted to a button.
        private void HighlightButton(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = ColorTranslator.FromHtml("#4d00a3");
        }

        // Event handler - changes backcolor of button to black.
        // Meant to be called when focus is shifted away from a button.
        private void UnhighlightButton(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = Color.Black;
        }

        // Event handler - closes the form.
        // Meant to be called by a click on the Close button.
        private void ClickClose(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}