﻿using System.Drawing;

namespace Taikong_Zhanshi
{
    class PurpleBug : Bug
    {
        // Constructor.
        public PurpleBug(int startFrame, Point location) : base(startFrame, location)
        {
            Frames = new Image[] { Properties.Resources.Purple_Bug_1, Properties.Resources.Purple_Bug_2, Properties.Resources.Purple_Bug_3 };
            this.Box.Image = Frames[CurrentFrame];
        }
    }
}