﻿using System;
using System.IO;
using WMPLib;

namespace Taikong_Zhanshi
{
    class MP3Player
    {
        // Properties.
        private WindowsMediaPlayer Player = null;
        private static string[] Tracks { get; set; } = null;

        // Constructor - Player plays a random song found in the default directory.
        public MP3Player()
        {
            LoadFiles();
            Random rand = new Random();

            if (TracksExist())
            {
                Player = new WindowsMediaPlayer() { URL = Tracks[rand.Next(0, Tracks.Length)] };
                FinishSetUp();
            }
        }

        // Constructor - Player plays song found at file name, if it exists.
        public MP3Player(string filename)
        {
            if (File.Exists(filename))
            {
                Player = new WindowsMediaPlayer() { URL = filename };
                FinishSetUp();
            }
        } 

        // Load in files from directory.
        private void LoadFiles()
        {
            if (!TracksExist())
            {
                try
                {
                    Tracks = Directory.GetFiles(Settings.DIRECTORYNAME);
                }
                catch (DirectoryNotFoundException)
                {
                    Tracks = null;
                }
            }
        }

        // Helper - true if Tracks contains any files, false otherwise.
        private bool TracksExist()
        {
            return (!(Tracks == null || Tracks.Length == 0));
        }

        // Sets audio to start of track and sets player to loop.
        private void FinishSetUp()
        {
            if (Player != null)
            {
                this.Player.controls.currentPosition = 0;
                this.Player.settings.setMode("loop", true);
            }
        }

        // Plays audio.
        public void Play()
        {
            if (Player != null)
            {
                this.Player.controls.play();
            }
        }

        // Stops audio.
        public void Stop()
        {
            if (Player != null)
            {
                this.Player.controls.stop();
            }
        }
    }
}