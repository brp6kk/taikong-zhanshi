﻿using System.Drawing;
using System.Windows.Forms;

namespace Taikong_Zhanshi
{
    class BugBullet : Bullet
    {
        // Constructor.
        public BugBullet(Bug bug) : base(Properties.Resources.EnemyShoot)
        {
            this.Box.Location = this.StartingLocation(bug.Box);
        }

        // Moves bullet vertically away from the bug.
        public override void MoveBullet()
        {
            this.Box.Location = new Point(this.Box.Location.X, this.Box.Location.Y + Settings.BUG_BULLET_MOVE);
        }

        // Positions bullet to start from the bottom of the bug.
        public override Point StartingLocation(PictureBox reference)
        {
            int referenceX = reference.Location.X;
            int referenceY = reference.Location.Y;
            int x = referenceX + (Settings.BUG_WIDTH / 2) - (Settings.BULLET_WIDTH / 2);
            int y = referenceY + Settings.BUG_HEIGHT;

            return (new Point(x, y));
        }
    }
}