﻿using System.Drawing;
using System.Windows.Forms;

namespace Taikong_Zhanshi
{
    abstract class Bullet
    {
        // Properties.
        public Image Image { get; private set; }
        public PictureBox Box { get; private set; } = new PictureBox { Size = new Size(3, 10) };

        // Constructor.
        public Bullet (Image image)
        {
            this.Image = image;
            this.Box.Image = image;
        }

        // Moves the bullet appropriately.
        public abstract void MoveBullet();

        // Calculates the appropriate location for the bullet based on the shooter and its location.
        public abstract Point StartingLocation(PictureBox reference);

        // Determines if the bullet has hit its target.
        public bool CollidesWith(PictureBox target)
        {
            return (this.Box.Bounds.IntersectsWith(target.Bounds));
        }
    }
}