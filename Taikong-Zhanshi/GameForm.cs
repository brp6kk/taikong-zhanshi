﻿using System;
using System.Windows.Forms;

namespace Taikong_Zhanshi
{
    public partial class GameForm : Form
    {
        // Properties.
        private Stage Stage;

        // Constructor.
        public GameForm()
        {
            InitializeComponent();
        }

        // Event handler - executes code to start up the form.
        // Meant to be called on a form load.
        private void FormLoad(object sender, EventArgs e)
        {
            Stage = new Stage(new Player(), this);
            Stage.OnLoad();
        }

        // Event handler - moves player and bullets.
        // Meant to be called by a tick of a timer with an interval of 1.
        private void TickFast(object sender, EventArgs e)
        {
            Stage.Tick2Actions();
            if (Stage.GameOver())
            {
                (new GameOverForm(Settings.Score, Settings.PlayerShotAccuracy(Stage.Army.NumAlive()))).Show();
                Stage.EndGame();
                this.Close();
            }
            else if (Stage.Complete())
            {
                Stage = Stage.NextStage();
                GC.Collect();
            }
        }

        // Event handler - moves bug army.
        // Meant to be called by a tick of a timer with an interval of 75.
        private void TickSlow(object sender, EventArgs e)
        {
            Stage.Tick1Actions();
        }

        // Event handler - key bindings.
        // Meant to be called on a key down event.
        private void DownKey(object sender, KeyEventArgs e)
        {
            Stage.KeyDownActions(e.KeyCode);
        }

        // Event handler - key bindings.
        // Meant to be called on a key up event.
        private void UpKey(object sender, KeyEventArgs e)
        {
            Stage.KeyUpActions(e.KeyCode);
        }
    }
}