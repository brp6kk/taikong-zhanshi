﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Taikong_Zhanshi
{
    class Stage
    {
        // Reference to form that stage is played on.
        private Form Form { get; set; }

        // Label that displays the score.
        private Label ScoreShow { get; set; } = new Label { Size = new Size(100, 25), Location = new Point(Settings.MAX_X - 70, Settings.MAX_Y - 15),
                                                            BackColor = Color.Transparent, ForeColor = Color.Lime, Font = new Font("OCR A Extended", 12),
                                                            TextAlign = ContentAlignment.BottomRight, Text = "" + Settings.Score};

        // Various aspects of stage.
        public Player Player { get; private set; }
        public Armada Army { get; private set; } = Armada.BuildDefaultArmada();
        public List<Bullet> Bullets { get; private set; } = new List<Bullet>();

        // Audio.
        public MP3Player Music = new MP3Player();

        // Constructor.
        public Stage(Player player, Form form)
        {
            this.Player = player;
            this.Form = form;
        }

        // Call in the Form's Load method - stage setup code.
        public void OnLoad()
        {
            // Removes all previous controls from the form.
            Form.Controls.Clear();

            // Adds player to from, displays how many lives it has.
            Form.Controls.Add(Player.Box);

            foreach (PictureBox lifeMarker in Player.LifeMarkers)
            {
                Form.Controls.Add(lifeMarker);
            }

            // Adds all bugs to the form.
            foreach (Bug[] row in Army.Army)
            {
                foreach (Bug bug in row)
                {
                    Form.Controls.Add(bug.Box);
                }
            }

            Form.Controls.Add(this.ScoreShow);

            Music.Play();
        }

        // Actions that should execute on every tick of a timer with an interval of 75.
        // Moves bug army.
        public void Tick1Actions()
        {
            Army.UpdateFrames();
            Army.Move();

            this.Army.StartAttack();

            BugShoot();
        }

        // Actions that should execute on every tick of a timer with an interval of 1.
        // Moves player and bullets.
        public void Tick2Actions()
        {
            // Moves player appropriately.
            Player.Move();

            // Moves bullets appropriately, removing any that have left the form's screen.
            foreach (Bullet bullet in Bullets)
            {
                bullet.MoveBullet();
            }
            RemoveOutsideBullets();

            // Performs actions if the event described in the method name occurred.
            PlayerShotBug();
            BugShotPlayer();
            BugHitPlayer();

            this.ScoreShow.Text = "" + Settings.Score;
        }

        // Actions that should execute on a keydown event.
        public void KeyDownActions(Keys key)
        {
            if (key == Keys.A || key == Keys.Left) // When A or Left is pressed, the player moves to the left
                Player.MovingLeft = true;
            else if (key == Keys.D || key == Keys.Right) // When D or Right is pressed, the player moves to the right
                Player.MovingRight = true;
            else if (key == Keys.Space || key == Keys.Enter) // When Space or Enter is pressed, the player shoots.
                PlayerShoot();
        }

        // Actions that should execute on a keyup event.
        public void KeyUpActions(Keys key)
        {
            if (key == Keys.A || key == Keys.Left) // When A or Left is no longer pressed, the player stops moving to the left
                Player.MovingLeft = false;
            else if (key == Keys.D || key == Keys.Right) // When D or Right is no longer pressed, the player stops moving to the right
                Player.MovingRight = false;
        }

        // Shoots a bullet originating from the player.
        // Shot only occurs if maximum number of shots has not yet been reached.
        private void PlayerShoot()
        {
            // Counts number of bullets originating from the player currently on the form.
            int count = 0;
            foreach (Bullet bullet in Bullets)
            {
                if (bullet is PlayerBullet) count++;
            }

            // If the player can shoot and the maximum has not been reached,
            // a new bullet is added to the stage.
            Bullet shot = this.Player.Shoot();
            if (shot != null && count < Settings.MAX_PLAYER_BULLETS)
            {
                Bullets.Add(shot);
                Form.Controls.Add(shot.Box);
                Settings.PlayerShotsFired++;
            }
        }

        // Attacking bug shoots when all of the conditions are correct.
        private void BugShoot()
        {
            // Gets attacking bug.
            Bug bug = Army.AttackingBug();

            // Counts number of bug bullets currently active.
            int count = 0;
            foreach (Bullet bullet in Bullets)
            {
                if (bullet is BugBullet) count++;
            }

            // Attacking bug shoots its shot if it exists, if it hasn't already shot, and if it's in the correct position.
            if (bug != null && count < Settings.MAX_BUG_BULLETS && Settings.BugShoot(bug.Box.Location.Y))
            {
                Bullets.Add(bug.Shoot());
                Form.Controls.Add(Bullets[Bullets.Count - 1].Box);
            }
        }

        // Checks to see if a player's bullet has collided with a bug, 
        // and takes appropriate actions if so.
        private void PlayerShotBug()
        {
            // Checks all bugs against all bullets.
            foreach (Bug[] row in Army.Army)
            {
                foreach (Bug bug in row)
                {
                    for (int index = 0; index < Bullets.Count; index++)
                    {
                        // If the bug exists and is alive, if the bullet came from the player, and if the two collide...
                        if (bug != null && bug.Alive && Bullets[index] is PlayerBullet && Bullets[index].CollidesWith(bug.Box))
                        {
                            // Kill bug and remove it and bullet from stage form.
                            bug.Kill();
                            Form.Controls.Remove(bug.Box);
                            Form.Controls.Remove(Bullets[index].Box);
                            Bullets.RemoveAt(index--); // Decrement necessary so that the next bullet isn't skipped over.
                            Settings.Score += Settings.KILL_BUG_SCORE; // Add to score.
                        }
                    }
                }
            }
        }

        // Checks to see if a bug's bullet has collided with the player, 
        // and takes appropriate actions if so.
        private void BugShotPlayer()
        {
            // Checks all bullets against the player.
            for (int index = 0; index < Bullets.Count; index++)
            {
                // If the bullet came from a bug and it collides with the player...
                if (Bullets[index] is BugBullet && Bullets[index].CollidesWith(Player.Box))
                {
                    // Kill the player and remove the bullet from the stage form.
                    this.PlayerDeath();
                    Form.Controls.Remove(Bullets[index].Box);
                    Bullets.RemoveAt(index--);
                }
            }
        }

        // Checks to see if attacking bug and the player have collided, and takes appropriate actions if so.
        private void BugHitPlayer()
        {
            Bug attackingBug = this.Army.AttackingBug();

            // If there is an attacking bug and if it collides with the player...
            if (attackingBug != null && attackingBug.Box.Bounds.IntersectsWith(Player.Bounds()))
            {
                // Kill the player and the bug.
                this.PlayerDeath();
                attackingBug.Kill();
                Form.Controls.Remove(attackingBug.Box);
                Settings.Score += Settings.KILL_BUG_SCORE; // Add to score.
            }
        }

        // Removes a life marker and kills the player.
        private void PlayerDeath()
        {
            // Removes a lifemarker from the form, as long as there is at least one to be removed.
            if (Player.LifeMarkers.Count > 0)
            {
                Form.Controls.Remove(Player.LifeMarkers[Player.LifeMarkers.Count - 1]);
            }

            Player.Kill();
        }

        // Removes all bullets that are outside of the form's bounds.
        private void RemoveOutsideBullets()
        {
            // Checks all bullets.
            for (int index = 0; index < Bullets.Count; index++)
            {
                // If the bullet goes past the top or bottom of the screen, remove it.
                if (!Settings.InYBounds(Bullets[index].Box.Location.Y))
                {
                    Form.Controls.Remove(Bullets[index].Box);
                    Bullets.RemoveAt(index--);
                }
            }
        }

        // Determines if the stage has been completed.
        public bool Complete()
        {
            return (this.Army.NumAlive() == 0);
        }

        // Determines if the entire game is over.
        public bool GameOver()
        {
            return (this.Player.Lives == 0);
        }

        // Creates the next stage, slightly harder than this one.
        public Stage NextStage()
        {
            // Stops music so that next stage can start a new track.
            Music.Stop();
            // Increases difficulty of game.
            Settings.UpdateSettings();

            // Creates new stage and sets it up properly.
            Stage newStage = new Stage(Player, Form);
            newStage.OnLoad();

            return (newStage);
        }

        // Stops music & resets settings.
        public void EndGame()
        {
            Music.Stop();
            Settings.ResetSettings();
        }
    }
}