﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Taikong_Zhanshi
{
    public partial class GameOverForm : Form
    {
        // Properties.
        private MP3Player Music = new MP3Player();
        private SaveData Data = new SaveData(Settings.FILENAME);
        private int Score;

        // Constructor.
        public GameOverForm(int score, double accuracy)
        {
            InitializeComponent();

            this.Score = score;
            lblStats.Text = score + "\n" + Settings.ToPercent(accuracy) + "%";
            
        }

        // Event handler - executes code to start up the form.
        // Meant to be called on a form load.
        private void FormLoad(object sender, EventArgs e)
        {
            Music.Play();

            Data.ReadFile();
            if (Data.HighScore(Score))
            {
                lblHighScorePrompt.Text = "New high score!\nEnter your initials:";
                lblHighScorePrompt.Height = 60;
                txtEnterName.Visible = true;
                btnClose.Text = "Enter";
            }
            else
            {
                int better = Data.SavedScores[9].Score;
                lblHighScorePrompt.Text = "Sorry, a new high score has not been achieved.\nScore to beat:\n" + better;
            }
        }

        // Event handler - key bindings.
        // Meant to be called on a key up event.
        private void PressKey(object sender, KeyEventArgs e)
        {
            //when enter is pressed, it has the equivilence as a Tab press
            //this is so that the user can move from textbox to quit button using controls they are already familiar with
            if (e.KeyCode == Keys.Enter)
                ClickClose(sender, e);
            else if (e.KeyCode == Keys.Down)
                SendKeys.Send("{TAB}");
        }

        // Event handler - closes the form.
        // Meant to be called by a click on the Close button.
        private void ClickClose(object sender, EventArgs e)
        {
            if (Data.HighScore(Score))
            {
                Data.NewScore(new SavedScore(txtEnterName.Text, Score));
                Data.WriteFile();
            }
            Music.Stop();
            (new mainForm()).Show();
            this.Close();
        }

        // Event handler - changes backcolor of button to purple.
        // Meant to be called when focus is shifted to a button.
        public void HighlightButton(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = ColorTranslator.FromHtml("#4d00a3");
        }

        // Event handler - changes backcolor of button to black.
        // Meant to be called when focus is shifted away from a button.
        public void UnhighlightButton(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = Color.Black;
        }
    }
}