﻿using System;
using System.Drawing;

namespace Taikong_Zhanshi
{
    static class Settings
    {
        public const int MIN_X = 5;
        public const int MAX_X = 305;
        public const int MIN_Y = 0;
        public const int MAX_Y = 400;
        public const int PLAYER_MIN_X = MIN_X - 13;
        public const int PLAYER_MAX_X = MAX_X - 13;
        public const int EXPLOSION_ANIMATION_INVERVAL = 4;

        public const int PLAYER_WIDTH = 50;
        public const int PLAYER_HEIGHT = 50;
        public const int PLAYER_WIDTH_REAL = PLAYER_WIDTH / 2;

        public const int BUG_ATTACK_Y = 160;

        public static readonly Image ENEMY_BULLET = Properties.Resources.EnemyShoot;
        public static readonly Image PLAYER_BULLET = Properties.Resources.Shoot;

        public const int BULLET_WIDTH = 3;
        public const int BULLET_HEIGHT = 10;

        public const int BUG_BULLET_MOVE = 6;
        public const int PLAYER_BULLET_MOVE = -6;

        public const int MAX_PLAYER_BULLETS = 3;
        public const int MAX_BUG_BULLETS = 1;

        public const int BUG_WIDTH = 25;
        public const int BUG_HEIGHT = 25;

        public const int KILL_BUG_SCORE = 30;
        public const int STAGE_CLEAR_SCORE = 50;

        public const int LIFE_MARKER_WIDTH = 12;
        public const int LIFE_MARKER_HEIGHT = 25;
        public const int LIFE_MARKER_X = 5;
        public const int LIFE_MARKER_Y = 385;

        public const int ENEMY_NON_ATTACK_MOVE = 5;

        public static int EnemyAttackXMove = 9;
        public static int EnemyAttackYMove = 3;
        public static int NumStagesCleared = 0;
        public static int Score = 0;
        public static int PlayerShotsFired = 0;
        public const int BUGS_IN_ARMY = 44;

        public const string FILENAME = "data.txt";
        public const string DIRECTORYNAME = "soundtrack";

        // Calculates number of bugs killed, both in previous stages & in current stage.
        public static int NumBugsKilled(int bugsLeft)
        {
            return (NumStagesCleared * BUGS_IN_ARMY + (BUGS_IN_ARMY - bugsLeft));
        }

        // Calculates accuracy of player, based on number of bugs killed & number of shots fired.
        public static double PlayerShotAccuracy(int bugsLeft)
        {
            return (Math.Round((double)NumBugsKilled(bugsLeft) / PlayerShotsFired, 2));
        }

        // Converts val to integer percentage.
        public static int ToPercent(double val)
        {
            try
            {
                return (Convert.ToInt32(Math.Round(100 * val)));
            }
            catch (OverflowException)
            {
                return (0);
            }
        }

        // Checks if x is between the allowed x bounds.
        public static bool InXBounds(int x)
        {
            return (x > Settings.MIN_X && x < Settings.MAX_X);
        }

        // Checks if y is between the allowed y bounds.
        public static bool InYBounds(int y)
        {
            return (y > Settings.MIN_Y && y < Settings.MAX_Y);
        }

        // Determines if a bug at vertical position y is at position to shoot.
        public static bool BugShoot(int y)
        {
            return (160 / EnemyAttackYMove == y / EnemyAttackYMove);
        }

        // Updates settings - meant to be called after a stage is over
        // to increase difficulty of game & update score appropriately.
        public static void UpdateSettings()
        {
            EnemyAttackXMove += 1;
            if (++NumStagesCleared % 3 == 0)
            {
                EnemyAttackYMove += 1;
            }
            Score += STAGE_CLEAR_SCORE;
        }

        // Resets variable settings.
        public static void ResetSettings()
        {
            EnemyAttackXMove = 9;
            EnemyAttackYMove = 3;
            NumStagesCleared = 0;
            Score = 0;
        }
    }
}
