﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Taikong_Zhanshi
{
    public partial class mainForm : Form
    {
        // Audio & visual properties.
        private MP3Player Music = new MP3Player("soundtrack/Rip_Off.mp3");
        private Image[] Images = {Properties.Resources.Stars01, Properties.Resources.Stars02, Properties.Resources.Stars03, Properties.Resources.Stars04,
                                  Properties.Resources.Stars05, Properties.Resources.Stars06, Properties.Resources.Stars07, Properties.Resources.Stars08,
                                  Properties.Resources.Stars09, Properties.Resources.Stars10, Properties.Resources.Stars11, Properties.Resources.Stars12,
                                  Properties.Resources.Stars13, Properties.Resources.Stars14, Properties.Resources.Stars15, Properties.Resources.Stars16,
                                  Properties.Resources.Stars17, Properties.Resources.Stars18, Properties.Resources.Stars19, Properties.Resources.Stars20,
                                  Properties.Resources.Stars21, Properties.Resources.Stars22, Properties.Resources.Stars23, Properties.Resources.Stars24,
                                  Properties.Resources.Stars25, Properties.Resources.Stars26, Properties.Resources.Stars27, Properties.Resources.Stars28,
                                  Properties.Resources.Stars29, Properties.Resources.Stars30, Properties.Resources.Stars31, Properties.Resources.Stars32,
                                  Properties.Resources.Stars33, Properties.Resources.Stars34, Properties.Resources.Stars35, Properties.Resources.Stars36,
                                  Properties.Resources.Stars37, Properties.Resources.Stars38, Properties.Resources.Stars39, Properties.Resources.Stars40,
                                  Properties.Resources.Stars41, Properties.Resources.Stars42, Properties.Resources.Stars43, Properties.Resources.Stars44,
                                  Properties.Resources.Stars45, Properties.Resources.Stars46, Properties.Resources.Stars47, Properties.Resources.Stars48,
                                  Properties.Resources.Stars49, Properties.Resources.Stars50};
        private int ImageIndex = 0;

        // Constructor.
        public mainForm()
        {
            InitializeComponent();
        }

        // Event handler - executes code to start up the form.
        // Meant to be called on a form load.
        private void FormLoad(object sender, EventArgs e)
        {
            Music.Play();
        }

        // Event handler - opens a new GameForm.
        // Meant to be called by a click on the Start button.
        private void ClickStart(object sender, EventArgs e)
        {
            Music.Stop();
            tmrBackground.Enabled = false;
            (new GameForm()).Show();
            // Hide rather than Close because closing original form would exit the program.
            this.Hide();
        }

        // Event handler - opens a new ControlsForm.
        // Meant to be called by a click on the Controls button.
        private void ClickControls(object sender, EventArgs e)
        {
            (new ControlsForm()).Show();
        }

        // Event handler - opens a new LeaderboardForm.
        // Meant to be called by a click on the Leaderboard button.
        private void ClickLeaderboard(object sender, EventArgs e)
        {
            (new LeaderboardForm()).Show();
        }

        // Event handler - opens a new CreditsForm.
        // Meant to be called by a click on the Credits button.
        private void ClickCredits(object sender, EventArgs e)
        {
            (new CreditsForm()).Show();
        }

        // Event handler - quits the program.
        // Meant to be called by a click on the Quit button.
        private void ClickQuit(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // Event handler - changes background image to next frame in animation.
        // Meant to be called by a tick of a timer.
        private void TimerTick(object sender, EventArgs e)
        {
            this.BackgroundImage = Images[ImageIndex++];

            if (ImageIndex >= Images.Length)
            {
                ImageIndex = 0;
            }
        }

        // Event handler - key bindings.
        // Meant to be called on a key up event.
        private void PressKey(object sender, KeyEventArgs e)
        {
             if (e.KeyCode == Keys.W)
                SendKeys.Send("{UP}");
             else if (e.KeyCode == Keys.S)
                SendKeys.Send("{DOWN}");
        }

        // Event handler - changes backcolor of button to purple.
        // Meant to be called when focus is shifted to a button.
        public void HighlightButton(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = ColorTranslator.FromHtml("#4d00a3");
        }

        // Event handler - changes backcolor of button to black.
        // Meant to be called when focus is shifted away from a button.
        public void UnhighlightButton(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = Color.Black;
        }
    }
}