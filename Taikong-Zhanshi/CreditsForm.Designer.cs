﻿namespace Taikong_Zhanshi
{
    partial class CreditsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreditsForm));
            this.label1 = new System.Windows.Forms.Label();
            this.lblCreditSelf = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSoundcloud = new System.Windows.Forms.Button();
            this.btnYoutube = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.DeepPink;
            this.label1.Font = new System.Drawing.Font("OCR A Extended", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(85, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(164, 37);
            this.label1.TabIndex = 0;
            this.label1.Text = "CREDITS";
            // 
            // lblCreditSelf
            // 
            this.lblCreditSelf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCreditSelf.Location = new System.Drawing.Point(12, 73);
            this.lblCreditSelf.Name = "lblCreditSelf";
            this.lblCreditSelf.Size = new System.Drawing.Size(310, 184);
            this.lblCreditSelf.TabIndex = 1;
            this.lblCreditSelf.Text = "Artwork, Code, Design:\r\nEric Yi-Min Chang\r\n\r\nAudio:\r\nYeltsin is Illuminati";
            // 
            // btnClose
            // 
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Location = new System.Drawing.Point(79, 349);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(170, 50);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.ClickClose);
            this.btnClose.Enter += new System.EventHandler(this.HighlightButton);
            this.btnClose.Leave += new System.EventHandler(this.UnhighlightButton);
            // 
            // btnSoundcloud
            // 
            this.btnSoundcloud.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSoundcloud.Location = new System.Drawing.Point(16, 199);
            this.btnSoundcloud.Name = "btnSoundcloud";
            this.btnSoundcloud.Size = new System.Drawing.Size(150, 50);
            this.btnSoundcloud.TabIndex = 1;
            this.btnSoundcloud.Text = "SoundCloud";
            this.btnSoundcloud.UseVisualStyleBackColor = true;
            this.btnSoundcloud.Click += new System.EventHandler(this.ClickSoundcloud);
            this.btnSoundcloud.Enter += new System.EventHandler(this.HighlightButton);
            this.btnSoundcloud.Leave += new System.EventHandler(this.UnhighlightButton);
            // 
            // btnYoutube
            // 
            this.btnYoutube.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnYoutube.Location = new System.Drawing.Point(168, 199);
            this.btnYoutube.Name = "btnYoutube";
            this.btnYoutube.Size = new System.Drawing.Size(150, 50);
            this.btnYoutube.TabIndex = 2;
            this.btnYoutube.Text = "YouTube";
            this.btnYoutube.UseVisualStyleBackColor = true;
            this.btnYoutube.Click += new System.EventHandler(this.ClickYoutube);
            this.btnYoutube.Enter += new System.EventHandler(this.HighlightButton);
            this.btnYoutube.Leave += new System.EventHandler(this.UnhighlightButton);
            // 
            // CreditsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(334, 411);
            this.ControlBox = false;
            this.Controls.Add(this.btnYoutube);
            this.Controls.Add(this.btnSoundcloud);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.lblCreditSelf);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("OCR A Extended", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(7, 5, 7, 5);
            this.Name = "CreditsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "太空战士";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PressKey);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblCreditSelf;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSoundcloud;
        private System.Windows.Forms.Button btnYoutube;
    }
}