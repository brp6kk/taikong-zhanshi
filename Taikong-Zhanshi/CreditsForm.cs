﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Taikong_Zhanshi
{
    public partial class CreditsForm : Form
    {
        // Constructor.
        public CreditsForm()
        {
            InitializeComponent();
        }

        // Event handler - opens browser and goes to link to SoundCloud.
        // Meant to be called by a click on the SoundCloud button.
        private void ClickSoundcloud(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://soundcloud.com/yeltsinisilluminati/tracks");
        }

        // Event handler - opens browser and goes to link to YouTube.
        // Meant to be called by a click on the YouTube button.
        private void ClickYoutube(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.youtube.com/user/TifgoCS2");
        }

        // Event handler - changes backcolor of button to purple.
        // Meant to be called when focus is shifted to a button.
        private void HighlightButton(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = ColorTranslator.FromHtml("#4d00a3");
        }

        // Event handler - changes backcolor of button to black.
        // Meant to be called when focus is shifted away from a button.
        private void UnhighlightButton(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = Color.Black;
        }

        // Event handler - closes the form.
        // Meant to be called by a click on the Close button.
        private void ClickClose(object sender, EventArgs e)
        {
            this.Close();
        }

        // Event handler - key bindings.
        // Meant to be called on a key up event.
        private void PressKey(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.W)
                SendKeys.Send("{UP}");
            else if (e.KeyCode == Keys.S)
                SendKeys.Send("{DOWN}");
            else if (e.KeyCode == Keys.A)
                SendKeys.Send("{LEFT}");
            else if (e.KeyCode == Keys.D)
                SendKeys.Send("{RIGHT}");
        }
    }
}